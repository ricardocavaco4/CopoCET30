﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace CopoCET30
{
    public partial class Form_Copo : Form
    {

        Copo CriarCopo = new Copo();

        public Form_Copo()
        {

            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ComboBox_Copo.Items.Add("Selecionar Copo");
            ComboBox_Copo.Items.Add("Copo de 30 ml");
            ComboBox_Copo.Items.Add("Copo de 250 ml");
            ComboBox_Copo.Items.Add("Copo de 400 ml");
            ComboBox_Copo.Items.Add("Copo de 600 ml");

            ComboBox_Bebida.Items.Add("Selecionar Bebida");
            ComboBox_Bebida.Items.Add("Cerveja");
            ComboBox_Bebida.Items.Add("Vinho");
            ComboBox_Bebida.Items.Add("Vodka");
            ComboBox_Bebida.Items.Add("Whiskey");
            ComboBox_Bebida.Items.Add("Água");
            ComboBox_Bebida.Items.Add("Café");
            ComboBox_Bebida.Items.Add("Sumo de Laranja");
            ComboBox_Bebida.Items.Add("Coca Cola");

        }

        private void Button_newCopo_Click(object sender, EventArgs e)
        {
            if(ComboBox_Copo.Text != "Selecione o copo")
            {
                if(ComboBox_Bebida.Text != "Selecione a bebida")
                {

                    Button_Encher.Visible = true;
                    Button_Esvaziar.Visible = true;
                    Label_Texto.Visible = true;
                    Numeric_Texto.Visible = true;
                    Button_SendCopo.Visible = true;
                    CriarCopo.Contem = 0.00;
                    CriarCopo.Capacidade = qtdcopo();
                    CriarCopo.Liquido = ComboBox_Bebida.Text;
                    Label_Texto.Text = CriarCopo.ToString();

                }
                else
                {
                    MessageBox.Show("Não selecionou uma bebida!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ComboBox_Bebida.Select();

                }
            }
            else
            {
                MessageBox.Show("Não selecionou um copo!", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ComboBox_Copo.Select();
            }
        }

        private void ComboBox_Copo_SelectedIndexChanged(object sender, EventArgs e)
        {
            MudarImagem_Copo();
        }

        private void ComboBox_Bebida_SelectedIndexChanged(object sender, EventArgs e)
        {
            MudarImagem_Bebida();
        }

        private void MudarImagem_Copo()
        {
            if(ComboBox_Copo.Text != "Selecionar Copo")
            {

                if (ComboBox_Copo.Text == "Copo de 30 ml")
                {
                    PictureBox_Copo.ImageLocation = @"Img\Copo3.jpg";
                }
                else if (ComboBox_Copo.Text == "Copo de 250 ml")
                {
                    PictureBox_Copo.ImageLocation = @"Img\Copo25.jpg";
                }
                else if (ComboBox_Copo.Text == "Copo de 400 ml")
                {
                    PictureBox_Copo.ImageLocation = @"Img\Copo40.jpg";
                }
                else if (ComboBox_Copo.Text == "Copo de 600 ml")
                {
                    PictureBox_Copo.ImageLocation = @"Img\Copo60.jpg";
                }
            }
            else
            {
                PictureBox_Copo.ImageLocation = "";
            }
        }

        private void MudarImagem_Bebida()
        {
            if (ComboBox_Bebida.Text != "Selecionar Bebida")
            {

                if (ComboBox_Bebida.Text == "Vodka")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_Vodka.png";
                }
                else if (ComboBox_Bebida.Text == "Cerveja")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_cerveja.jpg";
                }
                else if (ComboBox_Bebida.Text == "Vinho")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_Vinho.jpg";
                }
                else if (ComboBox_Bebida.Text == "Whiskey")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_Whiskey.jpg";
                }
                else if (ComboBox_Bebida.Text == "Água")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_Agua.jpg";
                }

                else if (ComboBox_Bebida.Text == "Café")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_Cafe.jpg";
                }

                else if (ComboBox_Bebida.Text == "Sumo de Laranja")
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_SumoLaranja.jpg";
                }
                else
                {
                    PictureBox_Bebida.ImageLocation = @"Img\B_CocaCola.jpg";
                }

            }
            else
            {
                PictureBox_Bebida.ImageLocation = "";
            }
        }

        private int qtdcopo()
        {
            if(ComboBox_Copo.SelectedIndex == 1)
            {
                return 30;
            }
            else if(ComboBox_Copo.SelectedIndex == 2)
            {
                return 250;
            }
            else if (ComboBox_Copo.SelectedIndex == 3)
            {
                return 400;
            }
            else if (ComboBox_Copo.SelectedIndex == 4)
            {
                return 600;
            }
            else
            {
                return 0;
            }
        }

        private void Button_Esvaziar_Click(object sender, EventArgs e)
        {
            int x;
            if(int.TryParse(Numeric_Texto.Text, out x))
            {
                CriarCopo.Esvaziar(Convert.ToInt16(x));
                Label_Texto.Text = CriarCopo.ToString();

            }
        }

        private void Button_Encher_Click(object sender, EventArgs e)
        {
            int x;
            if (int.TryParse(Numeric_Texto.Text, out x))
            {
                CriarCopo.Encher(Convert.ToInt16(x));
                Label_Texto.Text = CriarCopo.ToString();

            }
        }

        private void Buttton_SendCopo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Copo servido!", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            PictureBox_Bebida.ImageLocation = "";
            PictureBox_Copo.ImageLocation = "";
            ComboBox_Copo.SelectedIndex = 0;
            ComboBox_Bebida.SelectedIndex = 0;
            Numeric_Texto.Text = "";
            Numeric_Texto.Visible = false;
            Button_Encher.Visible = false;
            Button_Esvaziar.Visible = false;
            Label_Texto.Visible = false;
            Button_SendCopo.Visible = false;
            ComboBox_Copo.Select();
        }

        private void Button_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
