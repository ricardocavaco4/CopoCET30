﻿namespace CopoCET30
{
    partial class Form_Copo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label_SelectCopo = new System.Windows.Forms.Label();
            this.Label_SelectBebida = new System.Windows.Forms.Label();
            this.PictureBox_Copo = new System.Windows.Forms.PictureBox();
            this.PictureBox_Bebida = new System.Windows.Forms.PictureBox();
            this.Button_newCopo = new System.Windows.Forms.Button();
            this.ComboBox_Copo = new System.Windows.Forms.ComboBox();
            this.ComboBox_Bebida = new System.Windows.Forms.ComboBox();
            this.Button_Esvaziar = new System.Windows.Forms.Button();
            this.Button_Encher = new System.Windows.Forms.Button();
            this.Button_SendCopo = new System.Windows.Forms.Button();
            this.Label_Texto = new System.Windows.Forms.Label();
            this.Numeric_Texto = new System.Windows.Forms.NumericUpDown();
            this.Button_Exit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Copo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Bebida)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Numeric_Texto)).BeginInit();
            this.SuspendLayout();
            // 
            // Label_SelectCopo
            // 
            this.Label_SelectCopo.AutoSize = true;
            this.Label_SelectCopo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_SelectCopo.Location = new System.Drawing.Point(11, 249);
            this.Label_SelectCopo.Name = "Label_SelectCopo";
            this.Label_SelectCopo.Size = new System.Drawing.Size(219, 16);
            this.Label_SelectCopo.TabIndex = 1;
            this.Label_SelectCopo.Text = "Selecione o tamanho do copo:";
            // 
            // Label_SelectBebida
            // 
            this.Label_SelectBebida.AutoSize = true;
            this.Label_SelectBebida.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_SelectBebida.Location = new System.Drawing.Point(11, 278);
            this.Label_SelectBebida.Name = "Label_SelectBebida";
            this.Label_SelectBebida.Size = new System.Drawing.Size(148, 16);
            this.Label_SelectBebida.TabIndex = 3;
            this.Label_SelectBebida.Text = "Selecione a bebida:";
            // 
            // PictureBox_Copo
            // 
            this.PictureBox_Copo.Location = new System.Drawing.Point(14, 12);
            this.PictureBox_Copo.Name = "PictureBox_Copo";
            this.PictureBox_Copo.Size = new System.Drawing.Size(235, 220);
            this.PictureBox_Copo.TabIndex = 4;
            this.PictureBox_Copo.TabStop = false;
            // 
            // PictureBox_Bebida
            // 
            this.PictureBox_Bebida.Location = new System.Drawing.Point(255, 12);
            this.PictureBox_Bebida.Name = "PictureBox_Bebida";
            this.PictureBox_Bebida.Size = new System.Drawing.Size(235, 220);
            this.PictureBox_Bebida.TabIndex = 5;
            this.PictureBox_Bebida.TabStop = false;
            // 
            // Button_newCopo
            // 
            this.Button_newCopo.BackColor = System.Drawing.Color.White;
            this.Button_newCopo.Location = new System.Drawing.Point(17, 355);
            this.Button_newCopo.Name = "Button_newCopo";
            this.Button_newCopo.Size = new System.Drawing.Size(157, 46);
            this.Button_newCopo.TabIndex = 6;
            this.Button_newCopo.Text = "Criar Copo";
            this.Button_newCopo.UseVisualStyleBackColor = false;
            this.Button_newCopo.Click += new System.EventHandler(this.Button_newCopo_Click);
            // 
            // ComboBox_Copo
            // 
            this.ComboBox_Copo.FormattingEnabled = true;
            this.ComboBox_Copo.Location = new System.Drawing.Point(236, 249);
            this.ComboBox_Copo.Name = "ComboBox_Copo";
            this.ComboBox_Copo.Size = new System.Drawing.Size(206, 21);
            this.ComboBox_Copo.TabIndex = 7;
            this.ComboBox_Copo.Text = "Selecione o copo";
            this.ComboBox_Copo.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Copo_SelectedIndexChanged);
            // 
            // ComboBox_Bebida
            // 
            this.ComboBox_Bebida.FormattingEnabled = true;
            this.ComboBox_Bebida.Location = new System.Drawing.Point(236, 277);
            this.ComboBox_Bebida.Name = "ComboBox_Bebida";
            this.ComboBox_Bebida.Size = new System.Drawing.Size(206, 21);
            this.ComboBox_Bebida.TabIndex = 8;
            this.ComboBox_Bebida.Text = "Selecione a bebida";
            this.ComboBox_Bebida.SelectedIndexChanged += new System.EventHandler(this.ComboBox_Bebida_SelectedIndexChanged);
            // 
            // Button_Esvaziar
            // 
            this.Button_Esvaziar.BackColor = System.Drawing.Color.White;
            this.Button_Esvaziar.Location = new System.Drawing.Point(200, 370);
            this.Button_Esvaziar.Name = "Button_Esvaziar";
            this.Button_Esvaziar.Size = new System.Drawing.Size(65, 20);
            this.Button_Esvaziar.TabIndex = 11;
            this.Button_Esvaziar.Text = "Esvaziar";
            this.Button_Esvaziar.UseVisualStyleBackColor = false;
            this.Button_Esvaziar.Visible = false;
            this.Button_Esvaziar.Click += new System.EventHandler(this.Button_Esvaziar_Click);
            // 
            // Button_Encher
            // 
            this.Button_Encher.BackColor = System.Drawing.Color.White;
            this.Button_Encher.Location = new System.Drawing.Point(337, 370);
            this.Button_Encher.Name = "Button_Encher";
            this.Button_Encher.Size = new System.Drawing.Size(65, 20);
            this.Button_Encher.TabIndex = 12;
            this.Button_Encher.Text = "Encher";
            this.Button_Encher.UseVisualStyleBackColor = false;
            this.Button_Encher.Visible = false;
            this.Button_Encher.Click += new System.EventHandler(this.Button_Encher_Click);
            // 
            // Button_SendCopo
            // 
            this.Button_SendCopo.BackColor = System.Drawing.Color.White;
            this.Button_SendCopo.Location = new System.Drawing.Point(412, 359);
            this.Button_SendCopo.Name = "Button_SendCopo";
            this.Button_SendCopo.Size = new System.Drawing.Size(65, 40);
            this.Button_SendCopo.TabIndex = 14;
            this.Button_SendCopo.Text = "Mandar Copo";
            this.Button_SendCopo.UseVisualStyleBackColor = false;
            this.Button_SendCopo.Visible = false;
            this.Button_SendCopo.Click += new System.EventHandler(this.Buttton_SendCopo_Click);
            // 
            // Label_Texto
            // 
            this.Label_Texto.AutoSize = true;
            this.Label_Texto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Texto.Location = new System.Drawing.Point(11, 307);
            this.Label_Texto.Name = "Label_Texto";
            this.Label_Texto.Size = new System.Drawing.Size(22, 16);
            this.Label_Texto.TabIndex = 15;
            this.Label_Texto.Text = "- -";
            this.Label_Texto.Visible = false;
            // 
            // Numeric_Texto
            // 
            this.Numeric_Texto.Location = new System.Drawing.Point(277, 370);
            this.Numeric_Texto.Name = "Numeric_Texto";
            this.Numeric_Texto.Size = new System.Drawing.Size(54, 20);
            this.Numeric_Texto.TabIndex = 16;
            this.Numeric_Texto.Visible = false;
            // 
            // Button_Exit
            // 
            this.Button_Exit.BackColor = System.Drawing.Color.White;
            this.Button_Exit.Location = new System.Drawing.Point(14, 329);
            this.Button_Exit.Name = "Button_Exit";
            this.Button_Exit.Size = new System.Drawing.Size(65, 20);
            this.Button_Exit.TabIndex = 17;
            this.Button_Exit.Text = "Sair";
            this.Button_Exit.UseVisualStyleBackColor = false;
            this.Button_Exit.Click += new System.EventHandler(this.Button_Exit_Click);
            // 
            // Form_Copo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 411);
            this.Controls.Add(this.Button_Exit);
            this.Controls.Add(this.Numeric_Texto);
            this.Controls.Add(this.Label_Texto);
            this.Controls.Add(this.Button_SendCopo);
            this.Controls.Add(this.Button_Encher);
            this.Controls.Add(this.Button_Esvaziar);
            this.Controls.Add(this.ComboBox_Bebida);
            this.Controls.Add(this.ComboBox_Copo);
            this.Controls.Add(this.Button_newCopo);
            this.Controls.Add(this.PictureBox_Bebida);
            this.Controls.Add(this.PictureBox_Copo);
            this.Controls.Add(this.Label_SelectBebida);
            this.Controls.Add(this.Label_SelectCopo);
            this.Name = "Form_Copo";
            this.Text = "Copo";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Copo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_Bebida)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Numeric_Texto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Label_SelectCopo;
        private System.Windows.Forms.Label Label_SelectBebida;
        private System.Windows.Forms.PictureBox PictureBox_Copo;
        private System.Windows.Forms.PictureBox PictureBox_Bebida;
        private System.Windows.Forms.Button Button_newCopo;
        private System.Windows.Forms.ComboBox ComboBox_Copo;
        private System.Windows.Forms.ComboBox ComboBox_Bebida;
        private System.Windows.Forms.Button Button_Esvaziar;
        private System.Windows.Forms.Button Button_Encher;
        private System.Windows.Forms.Button Button_SendCopo;
        private System.Windows.Forms.Label Label_Texto;
        private System.Windows.Forms.NumericUpDown Numeric_Texto;
        private System.Windows.Forms.Button Button_Exit;
    }
}

